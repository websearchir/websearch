# Documentation: Web Search & Information Retrieval
## Project by Thorsten Knöller & Alexander Diete

### The idea

When we thought about the search on the page of our university, one idea came
to our minds:

> It would be cool to search for PDF/DOC/PPT documents

This could help students to find slides or things like the current curriculum
more easily. Therefore we decided to build a search engine specifically for
finding these types of documents.

### The technology

The project ist devided in basically two parts:

+ The crawl and indexing of the data
+ The back- and frontend to query the data.

For crawling the data we decided to use `nutch`. This allowed us to quickly
get the crawl without having to build a new crawler. The results were then
stored and indexed in `solr`. This way we have to advantage to already have
a working platform that can be queried.

When we decided to build the back- and frontend we chose `NodeJS` with the
express framework as our programming language of choice. It gave us the
advantage of developing really fast without having the trouble of a setting
up a tomcat or something equivalent.

For the frontend design we used bootstrap. It looks very clean and is versy
easy to use.

### The work

First things first we had to install and run the crawling-applications. It
became pretty clear, that this could not be done with the small virtual machine
provided to us. So we decided to use a bare-metal machine which belongs to the
`squareroots`. The biggest problems here were the interdependencies of the
different applications. Especially `nutch` is dependent on very old instances
of `solr` and `hbase` which caused some problems in the beginning. But after
some hassle we were able to let it run normally.

Our first crawl resulted in approximately 100.000 documents in total
(with less than 10% being pdf/doc/ppt documents). After tweaking the settings
to be a little more aggressive (we are only crawling in the intranet) we were
able to finally get around 350.000 documents with roughly 20.000 relevant for
our project. We also decided to share our crawl with the other teams to
minimize efforts.   
