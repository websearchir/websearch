var solr_agent = require('solr-client')
var config = require('./config')
var moment = require('moment')
var client = solr_agent.createClient(config.solr_url, config.solr_port, '', '/solr')
var Set = require("collections/set")
var fs = require('fs')
var readline = require('readline');

crawl()
function crawl() {
    var sets = new Set()
    var string = ''

//    var rd = readline.createInterface({
//        input: fs.createReadStream('urls'),
//        output: process.stdout,
//        terminal: false
//    })
//
//    rd.on('line', function (line) {
//        sets.add(line)
//    })

    var query = client.createQuery()
        .q('*')
        .matchFilterMulti('type', ['pdf', 'x-tika-msoffice'])
        .start(0)
        .rows(25000)
    client.search(query, function (err, obj) {
        obj.response.docs.forEach(function (entry) {
            sets.add(entry.url.split('/')[2])
        })
        console.log(sets.length)
        sets.forEach(function (entry) {
            //console.log(entry)
            string += entry + '\n'
        })
        fs.writeFile("urls", string, function (err) {
            if (err) {
                console.log(err);
            } else {
                console.log("The file was saved!");
            }
        })
    })


}
