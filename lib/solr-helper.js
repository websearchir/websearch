var solr_agent = require('solr-client')
var config = require('./config')
var moment = require('moment')
var domains = require('./urls')
var client = solr_agent.createClient(config.solr_url, config.solr_port, '', '/solr')


function query(data, cb) {
  var term = createFuzzyQuery(data.query)
  var date = getSearchTimeFrame(data.startDate, data.endDate)

  var query = client.createQuery()
    .q(term)
    .matchFilter('date', date)
    .matchFilterMulti('url', domains[data.subdomain])
    .matchFilterMulti('type', ['pdf', 'x-tika-msoffice'])
    .edismax()
    .qf({content: 1, title: 5, url: 3})
    .mm('3<90%25')
    .start(data.start * config.result_size)
    .rows(config.result_size)//

  client.search(query, function(err,obj) {
    if (err) return cb(err)

    var norm_data = obj.response.docs.map(function(x) {
      var d = moment(x.tstamp)
      if (d.isValid()) x.tstamp = d.format("dddd, MMMM Do YYYY, h:mm:ss a")
      if (!x.title) x.title = x.url
      x.abstract = genAbstract(x.content, x.title)
      return x
    })

    var pages = createPageArray(parseInt(data.start), obj.response.numFound)

    return cb(null, {
      docs: norm_data,
      numFound: obj.response.numFound,
      start: (obj.response.start / config.result_size),
      max: pages[pages.length - 1],
      pages: pages
    })
  })
}

function getSearchTimeFrame(startDate, endDate) {
  var date
  switch (startDate) {
    case 'DAY':
      date = '[NOW-1DAY TO '
      break
    case 'WEEK':
      date = '[NOW-7DAY TO '
      break
    case 'MONTH':
      date = '[NOW-1MONTH TO '
      break
    case 'YEAR':
      date = '[NOW-1YEAR TO '
      break
    case undefined:
      date = '[* TO '
      break
    case "":
      date = '[* TO '
      break
    case "Custom":
      date = '[* TO '
      break
    default:
      date = '[' + startDate + 'T00:00:00Z TO '
      break
  }
  if (endDate == undefined || endDate === '') {
    date += '*]'
  } else {
    date += endDate + 'T00:00:00Z]'
  }
  return date
}

function createFuzzyQuery(query) {
  var term = ""
  var i = 0
  var parts = query.split("\"")
  parts.forEach(function (part) {
    if (i % 2 == 0 && part != "") {
      part.split(" ").forEach(function(t) {
      if(t != ""){
        if(t.length < 4) {
          term += t + " "
        } else {
          term += t + config.fuzzy + " "
        }
      } else {
        term += " "
      }
      })
    } else {
      term += part
    }
    if (i != parts.length - 1)
      term += '\"'
      i++
  })
  return term
}

function genAbstract(content, title) {
  if(content.indexOf(title) == 0)
    content = content.slice(title.length+1)
  var tokkens = content.split(" ")
  return tokkens.slice(0,30).join(" ") + "..."
}

function createPageArray(start, numDocs) {
  var max = Math.floor(numDocs / config.result_size) + 1

  if (max <= 6) {
    var pages = new Array(max)
    for (var i=0; i < pages.length; i++)
      pages[i] = i
    return pages
  }

  if (parseInt(start) == 0)
    return ['0', '1', '...', max]

  if (parseInt(start) == 1)
    return ['0', '1', '2', '...', max]

  if (parseInt(start) == 2)
    return ['0', '1', '2', '3', '...', max]

  if (parseInt(start) == max)
    return ['0', '...', (max - 1), start]

  if (parseInt(start) == (max - 1))
    return ['0', '...', (max - 2), start, max]

  if (parseInt(start) == (max - 2))
    return ['0', '...', (max - 3), start, (max - 1), max]

  return ['0', '...', (parseInt(start) - 1), start, (parseInt(start) + 1), '...', max]
}

module.exports = {
  'query': query
}
