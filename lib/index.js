var solr = require('./solr-helper')
var h = require('./helpers.js')
var hbs = require('hbs')
var express = require('express')
var app = express()

// Configure handlebars
hbs.registerPartials(__dirname + '/../views/partials')
hbs.registerHelper('incIndex', h.helpers.incIndex)
hbs.registerHelper('isEqual', h.helpers.isEqual)
hbs.registerHelper('classForPage', h.helpers.classForPage)
hbs.registerHelper('nextItem', h.helpers.nextItem)
hbs.registerHelper('prevItem', h.helpers.prevItem)

app.set('view engine', 'hbs')
app.set('views', __dirname + '/../views')
app.use(require('body-parser')());
app.use(express.static(__dirname + '/../public'));

// Routes
app.get('/', function(req, res) {
  return res.render('index', {title:'Web search'})
})

app.get('/query', function(req, res) {
  if (!req.query.q) return res.render('error', {error:'query-term is missing'})
  var query = {
    query: req.query.q,
    startDate: req.query.startDate,
    endDate: req.query.endDate,
    subdomain: req.query.subdomain,
    start: req.query.s ? parseInt(req.query.s) : 0
  }
  solr.query(query, function(err, data) {
    if (err) return res.render('error', {error:'server-error'})
    return res.render('results', {
      term: query.query,
      results: data,
      startDate: req.query.startDate,
      endDate: req.query.endDate,
      subdomain: req.query.subdomain,
    })
  })
})

app.get('/about', function(req, res) {
  res.render('about', {})
})

app.listen(3333)
