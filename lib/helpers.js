module.exports = {
  helpers: {
    incIndex: function(index) {
      if (index == '...') return index
      return (parseInt(index) + 1)
    },
    isEqual: function(a, b, opts) {
      if(parseInt(a) == parseInt(b))
        return opts.fn(this);
      else
        return opts.inverse(this);
    },
    classForPage: function(active, elem) {
      if(elem == '...') return 'disabled'
      if(active == elem) return 'active'
      return ''
    },
    nextItem: function(elem, max) {
      if(parseInt(elem) == max) return max
      return parseInt(elem) + 1
    },
    prevItem: function(elem) {
      if(parseInt(elem) == 0) return 0
      return parseInt(elem) - 1
    }
  }
}
